[[_TOC_]]

### At GitLab

I am a [Senior Frontend Engineer](https://about.gitlab.com/job-families/engineering/development/frontend/#senior-frontend-engineer) in the [Utilization group](https://handbook.gitlab.com/handbook/engineering/development/fulfillment/#fulfillmentutilization-team) of the [Fulfillment sub-department](https://about.gitlab.com/handbook/engineering/development/fulfillment/). From 2023-01-23 to 2023-04-17, I enrolled in an [Internship for Learning](https://about.gitlab.com/handbook/people-group/promotions-transfers/#internship-for-learning) as a PM in [Fulfillment:Purchase](https://about.gitlab.com/handbook/engineering/development/fulfillment/purchase/).

### About me

My name is Angelo ([pronounced like _/ˈan.d͡ʒe.lo/_](https://app2.nameshouts.com/names/public/pronounce-angelo)). I am from [Enna](https://en.wikipedia.org/wiki/Enna), Sicily, Italy. I have lived in Munich, Bavaria, Germany since 2015. Since June 2023, I am back to my hometown. I am the proud legal guardian of [Tarta](https://about.gitlab.com/company/team-pets/#305-tarta).

#### Presence on the web

- [LinkedIn](https://www.linkedin.com/in/angelogulina/)
- [Twitter](https://twitter.com/angelogulina)
- [angelogulina.dev](https://angelogulina.dev/)

### Personal traits

Here are some facts about me to give you an idea of who (I think) I am:

- I like to listen to everyone's point of view. When doing it, I tend to nod and smile: it means I am listening not that I agree
- I am open-minded. I prefer to give ideas careful evaluation before accepting them
- I don't like absolutness and rigidity. Therefore, even if I am stubborn – it is really hard to convince me – I gladly accept suggestions and I am always eager to try things out
- I usually play the devil's advocate: there is always an angle that has not been evaluated and is worth to explore. Especially for topics I deem important, I tend to put interlocutors on trial: it is not personal, the aim is to get the best out of the conversation and reach a common ground
- I don't like commands. If someone is not able to argue their position, it is not a worthy one
- I reason schematically, it helps me see the evolution of an idea (e.g., I ask quesitons to try and understand how something got from _A_ to _B_)
- I believe in people and their abilities even, and especially, when they don't believe in themselves. Therefore, I often encourage them and try to help them out

### Collaboration

Here are some things to take into account when collaborating with me:

- I am always eager to offer help. Ask me anything anytime. Although I might not show it, I appreciate people saying "Thanks"
- I struggle to say "No", it is emotionally distressing. If it happens, I could not have decided otherwise
- I rely heavily on [slow-thinking](https://en.wikipedia.org/wiki/Thinking,_Fast_and_Slow) to be effective in my decision making. Therefore, I have a bias for any style of communication that helps me with that (e.g., written, async, ecc)
- I promote my own ideas and solutions poorly. When I do, I appreciate recognition. When in difficulty, I appreciate support and guidance
- I have an innate propensity for problem solving. I find it hard to tell if someone is asking for help or just sharing frustration. Please, consider making it clear to me, even if it seems awkward

### How to read my Google Calendar

I'm a [non-linear workday](https://about.gitlab.com/company/culture/all-remote/non-linear-workday/) practitioner. My working hours are roughly `07:00 – 18:00` (it doesn't mean I work ~ 11 hours). To help understand what I am doing at a certain moment of the day, see this helpful list of emojis. Although I try to keep Slack and Goolge Calendar in sync, always refer to the latter as the source of truth.

- 🍝, I am having lunch, or a break from work (sometimes I skip meals). I don't read emails or pings; I check Slack from mobile
- 👧, I am taking care of my daughter. I don't read emails or pings; I check Slack from mobile
- ⌛️, 🎸 I am focusing on personal development (e.g., studying, learning, etc). I don't read emails, pings or Slack
- 🤗, I am taking care of my mental health with a professional advisor. I don't read emails, pings or Slack
- 🎧, I am focusing and will most likely not accept meetings invitations for this slot. I read emails, pings or Slack
- ⛔️, busy. I don't read emails, pings or Slack

Other meetings I participate in appear as busy slots. Whenever the calendar is fee, you can assume I am working. I gladly accept meetings, coffee chats, etc. I will reply to emails, pings, and Slack.

I appreciate any feedabck and suggestions on how to make my schedule more comprehensible, and transparent for others.

### Emoji Pet Peeve

There's one emoji that particularly annoys me, mainly because I have assigned it a personal meaning: 👍 – I prefer 👌
